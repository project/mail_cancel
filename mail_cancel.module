<?php

/**
 * Implements hook_permission().
 *
 * @return array
 */
function mail_cancel_permission() {
  return array(
    'administer mail cancel' => array(
      'title' => t('Administer mail cancellation settings'),
      'description' => t('Manage mail domains which should be prevented from mailing.'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 * @return array
 */
function mail_cancel_menu() {
  $items = array();
  $items['admin/config/system/mail-cancel'] = array(
    'title' => 'Mail Cancel',
    'description' => 'Administer settings for cancellation of mails.',
    'access arguments' => array('administer mail cancel'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mail_cancel_admin_settings'),
  );
  return $items;
}

/**
 * Implementation of hook_mail_alter().
 *
 * @see drupal_mail()
 */
function mail_cancel_mail_alter(&$message) {
	$mail_cancel_to_regex = variable_get('mail_cancel_to_regex', '/@example.com/');
	$mail_cancel_to_regex_conditions = explode("\n", $mail_cancel_to_regex);
	
	$matched_regex = FALSE;
	foreach ($mail_cancel_to_regex_conditions as $regex) {
	  $regex = trim($regex);
	  $match = preg_match($regex, $message['to']);
	  
	  if ($match === 1) {
	    $matched_regex = $regex;
	    break;
	  }
	}
	
  if($matched_regex !== FALSE) {
    $message['send'] = FALSE;
    
    if(variable_get('mail_cancel_log', FALSE)) {
    	watchdog('mail_cancel', 'Cancelled mail for %email, subject was %subject', array('%email' => $message['to'], '%subject' => $message['subject']), WATCHDOG_NOTICE);
    }
    
    if(variable_get('mail_cancel_log_regex', FALSE)) {
    	watchdog('mail_cancel', 'Cancelled mail for %email, matched regex %regex', array('%email' => $message['to'], '%regex' => $matched_regex), WATCHDOG_DEBUG);
    }
  }
}

/**
 * Administrative settings.
 *
 * @return array
 *   An array containing form items to place on the module settings page.
 */
function mail_cancel_admin_settings($form, &$form_state) {
	$form['mail_cancel_to_regex'] = array(
		'#title' => t('Regex for the mails "to" field'),
		'#description' => t('Cancel sending of mails to "to" adresses which match this regex. You can provide multiple regex, start each one on a new line.'),
		'#type' => 'textarea',
		'#default_value' => variable_get('mail_cancel_to_regex', '/@example.com/'),
	);
	$form['mail_cancel_log'] = array(
		'#title' => t('Log cancelled mails'),
		'#description' => t('Receiver and subject will be logged.'),
		'#type' => 'checkbox',
		'#default_value' => variable_get('mail_cancel_log', FALSE),
	);
	$form['mail_cancel_log_regex'] = array(
		'#title' => t('Log the matched regex'),
		'#description' => t('Useful for debugging your regex.'),
		'#type' => 'checkbox',
		'#default_value' => variable_get('mail_cancel_log_regex', FALSE),
	);
	return system_settings_form($form);
}
